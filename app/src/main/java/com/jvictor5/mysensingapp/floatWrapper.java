package com.jvictor5.mysensingapp;

/**
 * Created by msi-user on 5/7/2017.
 */

public class floatWrapper extends Object {

    public float val;
    public float get() {
        return val;
    }
    public void put(float f) {
        this.val = f;
    }
    public floatWrapper() {}
    public floatWrapper(float f) {
        this.val = f;
    }
    public floatWrapper(double d) {this.val = (float)d;}
}
