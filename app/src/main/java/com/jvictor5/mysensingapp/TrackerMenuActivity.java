package com.jvictor5.mysensingapp;

import android.content.Context;
import android.net.ConnectivityManager;
import android.net.Network;
import android.net.NetworkInfo;
import android.net.wifi.ScanResult;
import android.net.wifi.WifiInfo;
import android.net.wifi.WifiManager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.List;

public class TrackerMenuActivity extends AppCompatActivity {
    private String filename = "Tracker.xml";
    static private WifiManager wm = null;
    //List<String[]> results = new ArrayList<>(); //to save wifi scan results
    //not necessary for final app
    //TextView text = (TextView)findViewById(R.id.present_count_text);

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_tracker_menu);
        TextView tv = (TextView)findViewById(R.id.present_count_text);
        tv.setText("Click to start leave monitor!");
    }

    public void onStartClick (View view){
        wm = (WifiManager)getApplicationContext().getSystemService(WIFI_SERVICE);
        List<ScanResult> sl = wm.getScanResults();
        List<String> mac_addrs = new ArrayList<>();
        String [] access_points = new String[sl.size()];
        WifiInfo wi = wm.getConnectionInfo();
        if(wi != null){
            leaveWifi(wi);

            Toast.makeText(this, "leave event", Toast.LENGTH_LONG).show();
        /*if (sl.size() > 0) {
            for (int i = 0; i < sl.size(); i ++){
                access_points[i] = sl.get(i).BSSID;
                mac_addrs.add(sl.get(i).BSSID);
            }
            leaveEvent(access_points);
            reenterEvent(access_points);*/
        } else {
            Toast.makeText(this, "No WiFi :(", Toast.LENGTH_SHORT).show();
        }
    }
    public void onResultsClick (View view){

    }

    public void leaveWifi (WifiInfo OG_connection){
        //boolean leave = false;
        WifiInfo wi;
        TextView tv = (TextView) findViewById(R.id.present_count_text);
        while(isNetworkAvailable()){
            wi = wm.getConnectionInfo();
            if (wi == null){
                return;
            } else if(!wi.getBSSID().equals(OG_connection.getBSSID())){
                return;
            } else {
                tv.append(".");
                try {
                    Thread.sleep(3000);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }
    }

    private boolean isNetworkAvailable () {
        ConnectivityManager cm = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo ni = cm.getActiveNetworkInfo();
        if (ni == null){
            return false;
        }
        return ni.isConnected();
    }

    public void leaveEvent(String [] access_points){
        TextView text = (TextView)findViewById(R.id.present_count_text);
        wm = (WifiManager)getApplicationContext().getSystemService(WIFI_SERVICE);
        boolean leave_event = false;
        //Integer[] scan_value = new Integer[access_points.length];
        int count = access_points.length;
        int present_count;
        List<ScanResult> sl;
        TextView tv = (TextView) findViewById(R.id.present_count_text);
        while(!leave_event){
            wm.startScan();
            text.setText("waiting");
            //try {
             //   Thread.sleep(10000);
            ////}catch (Exception e){2
            ///    e.printStackTrace();
            //}
            //Toast.makeText(this, "Done Sleeping", Toast.LENGTH_SHORT).show();

            do {
                text.append(".");
                try {
                    Thread.sleep(3000);
                } catch (Exception e){
                    e.printStackTrace();
                }
                sl = wm.getScanResults();
            } while(sl != null);

            Toast.makeText(this, "Got Results!", Toast.LENGTH_SHORT).show();
            present_count = 0;
            for(ScanResult sr : sl){
                if (Arrays.asList(access_points).contains(sr.BSSID)) present_count++;
            }
            Toast.makeText(this, "Got List!", Toast.LENGTH_SHORT).show();
            Toast t2 = Toast.makeText(getApplicationContext(), Double.toString(present_count), Toast.LENGTH_LONG);
            t2.show();
            tv.setText(present_count);
            leave_event = ((double)present_count/count) < 0.25;
        }
        Toast tl = Toast.makeText(getApplicationContext(), "Leave Event: " + Calendar.getInstance().toString(), Toast.LENGTH_LONG);
        tl.show();
    }


    public void reenterEvent(String [] access_points){
        wm = (WifiManager)getApplicationContext().getSystemService(WIFI_SERVICE);
        boolean reenter_event = false;
        //Integer[] scan_value = new Integer[access_points.length];
        int count = access_points.length;
        int present_count;
        List<ScanResult> sl;
        TextView tv= (TextView) findViewById(R.id.present_count_text);
        while(!reenter_event){
            wm.startScan();

            try {
                Thread.sleep(15000);
            }catch (Exception e){
                e.printStackTrace();
            }
            sl = wm.getScanResults();
            present_count = 0;
            for(ScanResult sr : sl){
                if (Arrays.asList(access_points).contains(sr.BSSID)) present_count++;
            }
            Toast t2 = Toast.makeText(getApplicationContext(), Double.toString(present_count), Toast.LENGTH_LONG);
            t2.show();
            tv.setText(present_count);
            reenter_event = ((double)present_count/count) > 0.75;
        }
        Toast tl = Toast.makeText(getApplicationContext(), "Reenter Event: " + Calendar.getInstance().toString(), Toast.LENGTH_LONG);
        tl.show();
    }
}
