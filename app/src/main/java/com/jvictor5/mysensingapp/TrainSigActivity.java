package com.jvictor5.mysensingapp;

import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v4.view.VelocityTrackerCompat;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.MotionEvent;
import android.view.VelocityTracker;
import android.view.View;
import android.widget.TextView;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;


public class TrainSigActivity extends AppCompatActivity {
    private static final String DEBUG_TAG = "Velocity";
    private VelocityTracker vt = null;
    private List<floatWrapper> velocity_values = new ArrayList<floatWrapper>();
    private String filename = "Sig_TS.csv";
    //private File f = new File(getApplicationContext().getFilesDir(), filename);

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_train_sig);
        int lines = 0;
        try {
            File f = new File(getApplicationContext().getFilesDir(), filename);
            if (f.exists()) {
                BufferedReader file = new BufferedReader(new FileReader(f));
                while (file.readLine() != null) lines++;
                file.close();
            }
        } catch (Exception e){
            e.printStackTrace();
        }
        TextView tv = (TextView) findViewById(R.id.train_count);
        tv.setText(lines);
    }
/*
    @Override
    public boolean onTouchEvent(MotionEvent event){
        int index = event.getActionIndex();
        int action = event.getActionMasked();
        int pointerId = event.getPointerId(index);

        switch (action) {
            case MotionEvent.ACTION_DOWN:
                if (vt == null) {
                    vt = VelocityTracker.obtain();
                } else {
                    vt.clear();
                }
                vt.addMovement(event);
                break;
            case MotionEvent.ACTION_MOVE:
                vt.addMovement(event);
                vt.computeCurrentVelocity(1000);
                //get the vector length
                floatWrapper fw = new floatWrapper(Math.sqrt(Math.pow(VelocityTrackerCompat.getXVelocity(vt, pointerId), 2)
                        + Math.pow(VelocityTrackerCompat.getYVelocity(vt, pointerId), 2)));
                velocity_values.add(fw);
                break;
            case MotionEvent.ACTION_CANCEL:
            case MotionEvent.ACTION_UP:
                vt.recycle();
                break;
        }

        return true;
    }

    public void onPositiveClick (View view){
        float[] to_write = new float[velocity_values.size() + 1];
        to_write[0] = 1;    //indicates positive instance
        for (int i = 0; i < velocity_values.size(); i++){
            to_write[i+1] = velocity_values.get(i).get();
        }
        try {
            File file = new File(getApplicationContext().getFilesDir(), filename);
            if (!file.exists()){
                file.createNewFile();
            }
            FileWriter fw = new FileWriter(file, true);
            BufferedWriter bw = new BufferedWriter(fw);
            bw.write("\n");
            bw.write(Arrays.toString(to_write).replace("[","").replace("]", ""));
            bw.close();
            fw.close();
        } catch(Exception e){
            e.printStackTrace();
        }
        Intent intent = new Intent(this, this.getClass());
        startActivity(intent);
    }
    public void onNegativeClick (View view){
        float[] to_write = new float[velocity_values.size() + 1];
        to_write[0] = 0;    //indicates negative instance
        for (int i = 0; i < velocity_values.size(); i++){
            to_write[i+1] = velocity_values.get(i).get();
        }
        try {
            File file = new File(getApplicationContext().getFilesDir(), filename);
            if (!file.exists()){
                file.createNewFile();
            }
            FileWriter fw = new FileWriter(file, true);
            BufferedWriter bw = new BufferedWriter(fw);
            bw.write("\n");
            bw.write(Arrays.toString(to_write).replace("[","").replace("]", ""));
            bw.close();
            fw.close();
        } catch(Exception e){
            e.printStackTrace();
        }
        Intent intent = new Intent(this, this.getClass());
        startActivity(intent);
    }
    public void onFinishedClick (View view){
        Intent intent = new Intent(this, SigMenuActivity.class);
        startActivity(intent);
    }*/
}
