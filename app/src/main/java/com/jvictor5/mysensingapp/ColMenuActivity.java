package com.jvictor5.mysensingapp;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;

import java.io.File;

public class ColMenuActivity extends AppCompatActivity {

    String filename = "Loc_Info.csv";
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_col_menu);
    }

    public void onChooseClick(View view){
        Intent intent = new Intent(this, CheckInActivity.class);
        startActivity(intent);
    }

    public void onCreateClick(View view){
        Intent intent = new Intent(this, ChooseLocationActivity.class);
        startActivity(intent);
    }
    public void onClearClick (View view){
        File file = new File(getFilesDir(), filename);
        if (file.exists()){
            file.delete();
        }
    }
}
