package com.jvictor5.mysensingapp;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        //Intent intent = new Intent(this, CalcResults.class);
        //startService(intent);
    }

    public void onSigMenuClick (View view){
        Intent intent = new Intent(this, SigMenuActivity.class);
        startActivity(intent);
    }

    public void onCoLocationClick(View view){
        Intent intent = new Intent(this, ColMenuActivity.class);
        startActivity(intent);
    }

    public void onTrackerClick (View view){
        Intent i = new Intent(this, TrackerMenuActivity.class);
        startActivity(i);
    }
}
