package com.jvictor5.mysensingapp;

import android.support.annotation.BoolRes;
import android.support.v4.view.VelocityTrackerCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.MotionEvent;
import android.view.VelocityTracker;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.util.ArrayList;
import java.util.List;

public class TestSigActivity extends AppCompatActivity {
    VelocityTracker vt = null;
    List<Double> vel_X = new ArrayList<>();
    List<Double> vel_Y = new ArrayList<>();
    private String filename = "Sig_TS.csv";
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_test_sig);
        findViewById(R.id.loading).setVisibility(View.GONE);
    }
    @Override
    public boolean onTouchEvent(MotionEvent event) {
        int index = event.getActionIndex();
        int action = event.getActionMasked();
        int pointerId = event.getPointerId(index);
        switch (action){
            case MotionEvent.ACTION_DOWN:
                if(vt ==null){
                    vt = VelocityTracker.obtain();
                } else{
                    vt.clear();
                }
                vt.addMovement(event);
                break;
            case MotionEvent.ACTION_MOVE:
                vt.addMovement(event);
                vt.computeCurrentVelocity(1000);
                vel_X.add((double) VelocityTrackerCompat.getXVelocity(vt, pointerId));
                vel_Y.add((double)VelocityTrackerCompat.getYVelocity(vt, pointerId));
                break;
            case MotionEvent.ACTION_UP:
            case MotionEvent.ACTION_CANCEL:
                vt.recycle();
                vt = null;
                break;
        }

        return true;
    }

    public void onClickTest (View view){
        //findViewById(R.id.loading).setVisibility(View.VISIBLE);
        double[] ts = new  double[vel_X.size()];
        for (int i = 0; i < vel_X.size(); i ++){
            ts[i] = Math.sqrt(Math.pow(vel_X.get(i), 2) + Math.pow(vel_Y.get(i), 2));
        }
        Toast t1 = Toast.makeText(getApplicationContext(), "Got vector!", Toast.LENGTH_SHORT);
        t1.show();
        double min = Double.POSITIVE_INFINITY;
        String min_index = "one";
        File file = new File(getFilesDir(), filename);
        if (file.exists()) {
            List<double []> ts_group = new ArrayList<>();
            List<String> categories = new ArrayList<>();
            try {
                BufferedReader bf = new BufferedReader(new FileReader(file));
                String line;
                while ((line = bf.readLine()) != null) {
                    String[] row = line.replace("\"", "").split(",");
                    String category = row[0];
                    double[] to_cmp = new double[row.length - 1];
                    for (int i = 1; i < row.length; i++) {
                        to_cmp[i - 1] = Double.parseDouble(row[i]);
                    }
                    categories.add(category);
                    ts_group.add(to_cmp);
                }
                bf.close();
                } catch (Exception e) {
                    e.printStackTrace();
                }
                t1 = Toast.makeText(getApplicationContext(), "Read File!", Toast.LENGTH_SHORT);
                t1.show();
                for ( int i = 0; i < ts_group.size(); i ++) {
                    double dist = myDTW.distance(ts, ts_group.get(i));
                    if (dist < min) {
                        min_index = categories.get(i);
                        min = dist;
                    }
                }
            TextView tv = (TextView) findViewById(R.id.test_result);
            tv.setText(min_index);

        }else {
            Toast t = Toast.makeText(getApplicationContext(), "No file :(", Toast.LENGTH_LONG);
            t.show();
        }
        vel_X = new ArrayList<>();
        vel_Y = new ArrayList<>();
        vt = null;
        //findViewById(R.id.loading).setVisibility(View.GONE);
    }
}
