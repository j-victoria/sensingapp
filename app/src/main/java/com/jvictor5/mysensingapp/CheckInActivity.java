package com.jvictor5.mysensingapp;

import android.content.Context;
import android.content.Intent;
import android.net.wifi.ScanResult;
import android.net.wifi.WifiManager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Toast;

import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.opencsv.CSVReader;
import com.opencsv.CSVWriter;

import org.apache.commons.lang3.ArrayUtils;

import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class CheckInActivity extends AppCompatActivity {
    String filename = "Loc_Info.csv";
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_check_in);
    }

    public void onCheckInClick (View view) {
        WifiManager wm = (WifiManager) getApplicationContext().getSystemService(Context.WIFI_SERVICE);
        wm.startScan();
        try{
            Thread.sleep(10000);
        }catch(Exception e){
            e.printStackTrace();
        }
        List<ScanResult> sl = wm.getScanResults();
        //Map<String, Object> feature_list = new HashMap();
        if (sl.size() > 0) {
            File file = new File(getFilesDir(), filename);
            String[] keys = new String[sl.size()];
            String[] values = new String[sl.size()];
            Integer[] scan1 = new Integer[sl.size()];
            Integer[] scan2 = new Integer[sl.size()];
            Arrays.fill(scan2, 0);
            Toast t = Toast.makeText(getApplicationContext(), "Scan List " + sl.size(), Toast.LENGTH_LONG);
            t.show();
            for (int i = 0; i < sl.size(); i++) {
                keys[i] = sl.get(i).BSSID;
                //values[i] = Integer.toString(sl.get(i).level);
                scan1[i] = sl.get(i).level;
            }

            wm.startScan();
            try{
                Thread.sleep(15000);
            } catch (Exception e){
                e.printStackTrace();
            }

            sl = wm.getScanResults();

            for (int i = 0; i < sl.size(); i++){
                if(Arrays.asList(keys).contains(sl.get(i).BSSID)){
                    scan2[Arrays.asList(keys).indexOf(sl.get(i).BSSID)] = sl.get(i).level;
                }
            }

            for(int i = 0; i < scan1.length; i++){
                if (scan1[i]!= 0 && scan2[i] != 0){
                    values[i]= Integer.toString((scan1[i] + scan2[i])/2);
                } else if (scan1[i] == 0){
                    values[i] = Integer.toString(scan2[i]);
                } else {
                    values[i] = Integer.toString(scan1[i]);
                }
            }

            if (file.exists()) {
                String[] addrs = null;
                try {
                    CSVReader reader = new CSVReader(new FileReader(file));
                    addrs = reader.readNext();
                    reader.close();
                } catch (Exception e) {
                    e.printStackTrace();
                }
                Toast t1 = Toast.makeText(getApplicationContext(), Arrays.toString(addrs), Toast.LENGTH_SHORT);
                t1.show();

                String[] rearranged_values = new String[addrs.length];
                Arrays.fill(rearranged_values, "0");

                int index = -1;
                for (int i = 0; i < addrs.length; i++) {
                    index = Arrays.asList(keys).indexOf(addrs[i]);
                    if (index > 0) {
                        rearranged_values[i] = values[index];
                    }
                }
                t1 = Toast.makeText(getApplicationContext(), "rearranged the values", Toast.LENGTH_SHORT);
                t1.show();
                List<Integer>indices = new ArrayList<>();
                boolean all_good = true;
                for(int i = 0; i < keys.length; i++){
                    all_good = Arrays.asList(addrs).contains(keys[i]);
                    if (!all_good) {
                        indices.add(i);
                    }
                }
                //if (indices.size() == 0) {
                    try {
                        CSVWriter writer = new CSVWriter(new FileWriter(file, true));
                        writer.writeNext(rearranged_values);
                        writer.close();
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                /*} else {
                    int new_size = addrs.length + indices.size();
                    String [] all_addresses = new String[indices.size()];
                    String [] all_new_vals = new String[indices.size()];
                    List<String[]> current_csv = new ArrayList<>();
                    List<String[]> new_csv = new ArrayList<>();
                    try{
                        CSVReader reader = new CSVReader(new FileReader(file));
                        current_csv = reader.readAll();
                        reader.close();

                    } catch (Exception e){
                        e.printStackTrace();
                    }
                    //int i = 0;
                    //for (; i < addrs.length; i ++ ){
                    //    all_addresses[i] = addrs[i];
                    //    all_new_vals[i] = rearranged_values[i];

                    //}
                    for (int i = 0; i < indices.size(); i++){
                        all_addresses[i] = keys[indices.get(i)];
                        all_new_vals[i] = values[indices.get(i)];
                    }
                    current_csv.remove(0);//remove the address row
                    String[] filler = new String[indices.size()];
                    Arrays.fill(filler, "0");
                    for(String [] inst : current_csv){
                        new_csv.add(ArrayUtils.addAll(inst, filler));
                    }
                    new_csv.add(0, ArrayUtils.addAll(addrs, all_addresses));
                    new_csv.add(ArrayUtils.addAll( rearranged_values, all_new_vals));
                    try{
                        CSVWriter writer = new CSVWriter(new FileWriter(file, false));
                        writer.writeAll(new_csv);
                        writer.close();
                    }catch (Exception e){
                        e.printStackTrace();
                    }
                }*/
            } else {
                try {

                    file.createNewFile();
                    CSVWriter cv = new CSVWriter(new FileWriter(file, true));
                    cv.writeNext(keys);
                    cv.writeNext(values);
                    cv.close();
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        } else {
            Toast t = Toast.makeText(getApplicationContext(), "No Wifi :(", Toast.LENGTH_LONG);
            t.show();
        }
    }
}
