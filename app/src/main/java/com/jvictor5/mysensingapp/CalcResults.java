package com.jvictor5.mysensingapp;

import android.app.Service;
import android.content.Intent;
import android.icu.text.DateFormat;
import android.os.FileObserver;
import android.os.IBinder;
import android.widget.Toast;

import com.opencsv.CSVWriter;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.Callable;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;

public class CalcResults extends Service {
    private String filename = "Sig_TS.csv";

    public CalcResults() {
    }

    @Override
    public IBinder onBind(Intent intent) {
        // TODO: Return the communication channel to the service.
        throw new UnsupportedOperationException("Not yet implemented");
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        final Toast toast = Toast.makeText(getApplicationContext(), "Service Started!", Toast.LENGTH_LONG);
        toast.show();
        final List<Boolean> categories = new ArrayList<Boolean>();
        final List<double[]> ds = new ArrayList<>();
        File file1 = new File(getFilesDir(), filename);
        while(file1.exists()) {
            Toast t1 = Toast.makeText(getApplicationContext(), "DTW Started!", Toast.LENGTH_LONG);
            t1.show();
            try {
                String line;
                BufferedReader br = new BufferedReader(new FileReader(file1));
                while ((line = br.readLine()) != null) {
                    String[] row = line.replace("\"", "").split(",");
                    categories.add(Boolean.parseBoolean(row[0]));
                    double[] temp = new double[row.length - 1];
                    for (int i = 1; i < row.length; i++) {
                        temp[i - 1] = Double.parseDouble(row[i]);
                    }
                    ds.add(temp);
                }
                br.close();
                int incorrect = 0;
                int total = categories.size();
                /*List<List<Future<Double>>> distance_mat = new ArrayList<List<Future<Double>>>();
                ExecutorService ex = Executors.newFixedThreadPool((int)Math.pow(total, 2));
                for (int i = 0; i < categories.size(); i++) {
                    List<Future<Double>> il = new ArrayList<>();
                    for (int j = 0; j < total; j++) {
                        Callable<Double> callable = new myDTW(ds.get(i), ds.get(j));
                        Future<Double> future = ex.submit(callable);
                        il.add(future);
                    }
                    distance_mat.add(il);
                }
                for (int i = 0; i < total; i++){
                    double min = Double.POSITIVE_INFINITY;
                    int index = i;
                    for (int j = 0; j < total; j++){
                        double cmp = Double.POSITIVE_INFINITY;

                        if (j != i){
                            cmp = distance_mat.get(i).get(j).get();
                        }
                        if (cmp < min){
                            min = cmp;
                            index = j;
                        }
                    }
                    if (categories.get(index) != categories.get(i))incorrect++;
                }
                */
                File file = new File(getFilesDir(), "res.csv");
                if (!file.exists()) {
                    file.createNewFile();
                }
                CSVWriter cv = new CSVWriter(new FileWriter(file));
                cv.writeNext(new String[]{"sig", Double.toString((double) incorrect / total)});
                cv.close();
                Toast t = Toast.makeText(getApplicationContext(), "DTW Finished!", Toast.LENGTH_LONG);
                t.show();
            } catch (Exception e) {
                e.printStackTrace();
            }
            try {
                Thread.sleep(1000);
            }catch(Exception e){
                e.printStackTrace();
            }
        }
        return START_STICKY;
    }
}
