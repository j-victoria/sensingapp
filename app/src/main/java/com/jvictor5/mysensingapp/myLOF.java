package com.jvictor5.mysensingapp;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.List;

/**
 * Created by msi-user on 5/8/2017.
 */

public class myLOF {
    static public Boolean[] LOF (List<Integer[]> ds){
        Boolean [] rv = new Boolean[ds.size()];
        Integer k = ds.size()/3;
        List<Integer[]> dis = distance_matrix(ds);
        List<int[]> k_n = new ArrayList<>();
        double[] ld = new double[ds.size()];
        Double[] lf = new Double[ds.size()];

        for (int i = 0; i < ds.size(); i++){
            k_n.add(N_k(dis.get(i), k));
        }

        for (int i = 0; i < dis.size(); i++){
            ld[i] = (lrd(dis, i, k_n));
        }

        for (int i = 0; i < ld.length; i++) {
            lf[i] = (lof(ld, i, k_n));
        }

        for (int i = 0; i < lf.length; i++){
            rv[i] = (lf[i] > 2.0);
        }

        return rv;
    }

    static public List<Integer []> distance_matrix (List<Integer[]> ds){
        Integer[][] rv = new Integer[ds.size()][ds.size()];

        for (int i = 0;  i < ds.size(); i++){
            for (int j = 0; j < ds.size(); j++){
                rv[i][j] = rv[j][i] = (manhattan_distance(ds.get(i), ds.get(j)));
            }
        }

        return Arrays.asList(rv);
    }

    static public Integer manhattan_distance(Integer[] vec1, Integer[] vec2){
        Integer dist = 0;
        for (int i = 0; i < vec1.length; i++){
            dist += Math.abs(vec1[i] - vec2[i]);
        }
        return dist;
    }

    static public int[] N_k (Integer[] dp, Integer k){
        List<Integer> sorted = new ArrayList<>(Arrays.asList(dp));

        Collections.sort(sorted);

        int[] smallest = new int[k];
        for (int i = 1; i <= k; i++) {  //start at 1 because 0 should be itself
            smallest[i - 1] = (Arrays.asList(dp).indexOf(sorted.get(i)));
        }

        return smallest;
    }
    static public Integer k_distance (Integer[] dp, Integer k){
        int[] k_nn = N_k(dp, k);
        return dp[(k_nn[k - 1])];
    }

    static public  int k_distance(Integer[] dp, int[] k_nn){
        return dp[(k_nn[k_nn.length - 1])];
    }

    static public Integer sum_reachability_distance(List<Integer[]> dist_mat, Integer instance, List<int[]> k_nn){
        Integer distance = 0;
        for (Integer b : k_nn.get(instance)){
            distance += Math.max(dist_mat.get(instance)[b], k_distance(dist_mat.get(b), k_nn.get(b)));
        }
        return distance;
    }

    static public Double lrd(List<Integer[]> dist_mat, Integer instance, List<int[]> k_nn){
        return 1.0 / (sum_reachability_distance(dist_mat, instance, k_nn) / k_nn.get(instance).length);
    }

    static public Double lof(double[] local_lrd, Integer instance, List<int[]> k_nn){
        Double top = 0.0;
        for (Integer b : k_nn.get(instance)){
            top += local_lrd[b];
        }
        return (top / k_nn.get(instance).length) / local_lrd[instance];
    }
}
