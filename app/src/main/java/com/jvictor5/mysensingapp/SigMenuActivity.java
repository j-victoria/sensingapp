package com.jvictor5.mysensingapp;

import android.content.Intent;
import android.graphics.Path;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;

import java.io.File;

public class SigMenuActivity extends AppCompatActivity {

    private String filename = "Sig_TS.csv";
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sig_menu);
    }

    public void onTrainClick (View view){
        Intent intent = new Intent(this, TrainActivity.class);
        startActivity(intent);
    }

    public void onTestClick (View view){
        Intent intent = new Intent(this, TestSigActivity.class);
        startActivity(intent);
    }
    public void onResultsClick(View view){
        Intent intent = new Intent(this, VeiwSigResultsActivity.class);
        startActivity(intent);
    }
    public void onClearClick(View view){
        File file = new File (getFilesDir(), filename);
        try{
            if (file.exists()){
                file.delete();
            }
        }catch (Exception e){
            e.printStackTrace();
        }
    }
}
