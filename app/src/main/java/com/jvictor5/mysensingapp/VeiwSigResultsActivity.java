package com.jvictor5.mysensingapp;

import android.graphics.Color;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.TextView;

import com.jjoe64.graphview.GraphView;
import com.jjoe64.graphview.series.DataPoint;
import com.jjoe64.graphview.series.LineGraphSeries;
import com.opencsv.CSVReader;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.lang.reflect.Array;
import java.sql.Time;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.Map;

import static java.lang.Thread.sleep;

public class VeiwSigResultsActivity extends AppCompatActivity {
    private String filename = "Sig_TS.csv";
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_veiw_sig_results);
        TextView tv = (TextView) findViewById(R.id.sig_results_text);

        //List<TimeSeries> ds = new ArrayList<TimeSeries>();

        File file = new File(getFilesDir(), filename);
        //List<ArrayList<Double>> inst = new ArrayList<ArrayList<Double>>();
        //List<Boolean> classes = new ArrayList<Boolean>();
        List<double [] > data = new ArrayList<>();
        int line = 0;
        /*try {
            File fn = new File (getFilesDir(), "res.csv");
            if (fn.exists()){
                CSVReader cv = new  CSVReader(new FileReader(file));
                String[] ln;
                while((ln = cv.readNext())!= null){
                    if (ln[0] == "sig"){
                        tv.append("CV error rate: " + ln[1]);
                    }
                }
                cv.close();
            }

        }catch (Exception e){
            e.printStackTrace();
        }*/
        List<String> categories = new ArrayList<>();
        GraphView graph = (GraphView) findViewById(R.id.sig_graph);
        try {
            BufferedReader reader = new BufferedReader(new FileReader(file));
            String nextLine;
            while((nextLine = reader.readLine()) != null){
                //classes.add(Boolean.parseBoolean(nextLine[0]));
                //tv.append(nextLine + "\n");
                String [] row = nextLine.replace("\"", "").split(",");
                //tv.append(row[0]+ "\n");
                //ds.add(new TimeSeries())
                DataPoint [] tmp = new DataPoint[row.length - 1];
                //classes.add(Boolean.parseBoolean(row[0]));
                double [] point = new double[row.length - 1];
                for (int i = 1; i < row.length; i++){
                    //temp.add(Double.parseDouble(row[i]));
                    tmp[i-1] = new DataPoint( i ,Double.parseDouble(row[i]));
                    point[i - 1] = Double.parseDouble(row[i]);
                }
                //inst.add(temp);
                LineGraphSeries<DataPoint> series = new LineGraphSeries<>(tmp);
                categories.add(row[0]);
                //tv.append(row[0]);
                if (row[0].equals("one")){
                    series.setColor(Color.GREEN);
                } else if (row[0].equals("two")) {
                    series.setColor(Color.RED);
                } else {
                    series.setColor(Color.BLUE);
                }
                graph.addSeries(series);
                line++;
                data.add(point);
            }
            reader.close();
        }catch (Exception e){
            e.printStackTrace();
        }
        tv.append("There are " + line + " instances \n");
        Double[][] dist = new Double[line][line];
        Double temp[] = new Double[line];
        int inner_iter; int outter_iter = 0;
        for (;outter_iter < line; outter_iter++){
            inner_iter = 0;
            for (;inner_iter <= outter_iter; inner_iter++){
                dist[outter_iter][inner_iter] = dist[inner_iter][outter_iter] = (myDTW.distance(data.get(outter_iter), data.get(inner_iter)));
            }
        }
        int total = line;
        double incorrect = 0.0;
        int index;
        double min;

        for (int i = 0; i < line; i++){
            index = -1;
            min = Double.POSITIVE_INFINITY;
            for (int j = 0; j < line; j ++) {
                if (i != j) {
                    //tv.append("\n" + dist[i][j]);
                    if (dist[i][j] < min && dist [i][j] != 0.0) {
                        min = dist [i][j];
                        index = j;
                    }
                }
            }
            if (!categories.get(i).equals(categories.get(index))){
                incorrect ++;
                //tv.append("\n" + i  + "\n" + categories.get(i) +"\n "+ Double.toString(min) + "\n" + Integer.toString(index) + " " + categories.get(index));
            }
            //tv.append("\n "+ Double.toString(min) + "\n" + Integer.toString(index));
        }
        tv.append("\n LOO-CV error rate: " + incorrect/total);
/*
        //List <Instance> in = new ArrayList<Instance>();
        List<double[]> arr = new ArrayList<>();
        for (int i = 0; i < inst.size(); i ++){
            double [] me = new double[inst.get(i).size()];
            //TimeSeries ts = new TimeSeries(1);
            for (int j  = 0; j < inst.get(i).size(); j++){
                me[j] = inst.get(i).get(j);

            }
            arr.add(me);
            //ds.add(new TimeSeries(new DenseInstance(me)));
            //in.add(new DenseInstance(me));
        }

        double[][] distances = new double[inst.size()][inst.size()];
        //distance matrix
        for (int i = 0; i < arr.size(); i ++){
            int min_index = i;
            double min = 100000000;
            for (int j = 0; j < arr.size(); j++){
                if (i != j){
                    double cmp = myDTW.distance_alt(arr.get(i), arr.get(j));
                    //DTWSimilarity dtw = new DTWSimilarity();
                    //double cmp = dtw.measure(in.get(i), in.get(j));
                    //double cmp = DTW.getWarpDistBetween(ds.get(i), ds.get(j)); //measure(i_i, i_j);
                    if (cmp < min){
                        min = cmp;
                        min_index = j;
                    }
                }
            }
            tv.append(min_index + "\n");
            //try {sleep(10000);}
            //catch (Exception e){
            //    e.printStackTrace();
            //}
        }
/*
        float incorrect = 0;
        for (int i = 0; i < distances.length; i++){
            int min_index = i;
            double min = 1000000;
            for (int j = 0; j < distances[i].length; j++){
                if (j != i && distances[i][j] < min){
                    min = distances[i][j];
                    min_index = j;
                }
            }
            tv.append("For row: " + i + " closest is: " + min_index + "\n" );
            if (classes.get(min_index) != classes.get(i)) {
                incorrect++;
                tv.append("Which was incorrect! \n");
            }
        }
        tv.append("Leave-One-Out CV error: " + incorrect/line);
    */
    }
}
