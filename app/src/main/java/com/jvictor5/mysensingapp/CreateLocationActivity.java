package com.jvictor5.mysensingapp;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;

public class CreateLocationActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_create_location);
    }

    public void onSubmitClick(View view){
        String value = ((EditText)findViewById(R.id.loc_name)).getText().toString();
        Intent intent = new Intent(this, CheckInActivity.class);
        intent.putExtra("LOCATION", value);
        startActivity(intent);
    }
}
