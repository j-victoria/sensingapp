package com.jvictor5.mysensingapp;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.opencsv.CSVReader;

import java.io.File;
import java.io.FileReader;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Map;

public class ChooseLocationActivity extends AppCompatActivity {

    String filename = "Loc_Info.csv";
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_choose_location);
        //List<Integer[]> dataSet = new ArrayList<>();
        List<String[]> inbetween = new ArrayList<>();
        File file = new File(getFilesDir(),filename);
        if (file.exists()) {

            try {
                CSVReader reader = new CSVReader(new FileReader(file));
                inbetween = reader.readAll();
                reader.close();

            } catch (Exception e) {
                e.printStackTrace();
            }
            Integer[][] vals = new Integer[inbetween.size() - 1][inbetween.get(0).length];
            //Integer[] tmp = new Integer[inbetween.get(1).length];   //they should all be the same length anyway
            for (int i = 1; i < inbetween.size(); i++) {//first line is values

              for (int j = 0; j < inbetween.get(i).length; j++) {
                    //tmp[j] = (Integer.parseInt(inbetween.get(i)[j]));
                    System.out.print(inbetween.get(i)[j]);
                    vals[i - 1][j] = Integer.parseInt(inbetween.get(i)[j]);
                }
                //dataSet.add(tmp);
            }

            Boolean[] outliers = myLOF.LOF(Arrays.asList(vals));
            //List<Boolean> outliers = myLOF.LOF(dataSet);

            //Integer[][] dist = new Integer[vals.length][vals.length];
            //for (int i = 0; i < dist.length; i ++){
            //    for (int j = 0; j <= i; j++){
            //        dist[i][j] = dist[j][i] = 0;
            //        for (int k = 0; k < vals[i].length; k ++) {
            //            //dist[i][j] = dist[j][i] += (int)Math.pow(vals[i][k] - vals[j][k], 2);
            //            //causes failure?
            //        }
            //    }
            //}

            //List<String>  last = new ArrayList<>();
            //for (int i = 0; i < inbetween.size(); i ++){
            //    last.add(Arrays.toString(inbetween.get(i)));
            //}
            //Integer[][] dist_mat = new Integer[inbetween.size()][inbetween.size()];
            //for (int i = 0; i < inbetween.size(); i ++){
            //    for (int j = 0; j <= i; j++){
            //        dist_mat[i][j] = dist_mat[j][i] = myLOF.manhattan_distance(dataSet.get(i), dataSet.get(j));
            //    }
            //}
            //ArrayAdapter adapter = new ArrayAdapter(getApplicationContext(), R.layout.activity_choose_location, inbetween);
            //ListView lv = (ListView) findViewById(R.id.choose_loc);
            //lv.setAdapter(adapter);
            /*List<Boolean> bools = new ArrayList<>();
            String val1 = null, val2 = null, val3 = null;
            Boolean decider;
            for (String[] inst : inbetween){
                decider = false;
                val1 = inst[0];
                val2 = inst[1];
                val3 = inst[2];
                if (val1 == "0" && val2 == "0"){
                    decider = true;
                }else if (val1 == "0" && val3 == "0"){
                    decider = true;
                } else if (val2 == "0" && val3 == "0"){
                    decider = true;
                }
                bools.add(decider);
            }*/
            /*Toast t4 = Toast.makeText(getApplicationContext(), val1 + " " + val2 + " " + val3, Toast.LENGTH_LONG);*/
            TextView tv = (TextView)findViewById(R.id.info);
            for(int i = 1; i < inbetween.size(); i++){
                tv.append("\n" + outliers[i-1]);
            }
        } else {
            Toast t = Toast.makeText(getApplicationContext(), "No file!", Toast.LENGTH_LONG);
            t.show();
            String[] list = new String [5];
            for (int i = 0; i < 5; i++){
                list[i] = "Nothing!";
            }
            //ArrayAdapter adapter = new ArrayAdapter(this, R.layout.activity_choose_location, list);

            //ListView lv = (ListView) findViewById(R.id.choose_loc);
            //lv.setAdapter(adapter);
            TextView tv = (TextView)findViewById(R.id.info);
            for(int i = 0; i < list.length; i++){
                tv.append("\n" + list[i]);
            }
        }
    }


}
