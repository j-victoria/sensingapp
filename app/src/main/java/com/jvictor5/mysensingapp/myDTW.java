package com.jvictor5.mysensingapp;

import android.os.Process;

import java.lang.reflect.Array;
import java.util.Collection;
import java.util.Collections;
import java.util.concurrent.Callable;

/**
 * Created by msi-user on 5/7/2017.
 */

public class myDTW  {

    private double[] ts1;
    private double[] ts2;



    public myDTW (double[] ts1, double[]ts2){
        this.ts1 = ts1;
        this.ts2 = ts2;

    }


    static public double distance (double[] ts1, double[] ts2){
        int n = ts1.length + 1;
        int m = ts2.length + 1;
        double [][] dist_array = new double[n][m];
        for (int i = 0; i < n; i++){
            dist_array[i][0] = 1000000000;
        }
        for (int i = 0; i < m; i++){
            dist_array[0][i] = 1000000000;
        }
        dist_array[0][0] = 0;

        for (int i = 1; i < n; i++){
            for (int j = 1; j < m; j++){
                double cost = Math.abs(ts1[i - 1] - ts2[j - 1]);
                dist_array[i][j] = cost + min_of_3(dist_array[i][ j-1], dist_array[i - 1][j], dist_array[i -1][j - 1]);
                //System.out.println(" j: " + j);
            }
            //System.out.println(" i: "+i);
        }

        return dist_array[n - 1][m - 1];
    }

    static private double min_of_3(double d1, double d2, double d3){
        if (d1 <= d2 && d1 <= d3) {
            return d1;
        } else if (d2 <= d1 && d2 <= d3){
            return d2;
        } else {
            return d3;
        }
    }

    static public double distance_alt(double[] ts1, double[] ts2){
        return distance(ts1, ts1.length -1, ts2, ts2.length - 1);
    }

    static public double distance(double[] ts1, int i, double[] ts2, int j){
        if (i == -1 && j == -1){
            return 0;
        } else if (i == -1 || j == -1){
            return Double.POSITIVE_INFINITY;
        } else {
            return Math.abs(ts1[i] - ts2[j]) + min_of_3(distance(ts1, i -1, ts2, j), distance(ts1, i, ts2, j -1), distance(ts1, i-1, ts2, j-1));
        }

    }
}
