package com.jvictor5.mysensingapp;

import android.content.Intent;
import android.support.v4.view.VelocityTrackerCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.MotionEvent;
import android.view.VelocityTracker;
import android.view.View;
import android.widget.TextView;

import com.opencsv.CSVWriter;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.List;

public class TrainActivity extends AppCompatActivity {
    private String filename = "Sig_TS.csv";
    private VelocityTracker vt = null;
    private List<Double> vel_X  = new ArrayList<Double>();
    private List<Double> vel_Y  = new ArrayList<Double>();
    private Integer count;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_train);
        int list = 0;
        TextView tv = (TextView)findViewById(R.id.train_count);
        try{
            File file = new File(getFilesDir(), filename);
            if (file.exists()){
                BufferedReader br = new BufferedReader(new FileReader(file));
                while(br.readLine() != null) list ++;
                br.close();
            }
        }catch(Exception e){
            e.printStackTrace();
        }
        count = list;
        tv.setText(Integer.toString(list));
    }

    @Override
    public boolean onTouchEvent(MotionEvent event) {
        int index = event.getActionIndex();
        int action = event.getActionMasked();
        int pointerId = event.getPointerId(index);
        switch (action){
            case MotionEvent.ACTION_DOWN:
                if(vt ==null){
                    vt = VelocityTracker.obtain();
                } else{
                    vt.clear();
                }
                vt.addMovement(event);
                break;
            case MotionEvent.ACTION_MOVE:
                vt.addMovement(event);
                vt.computeCurrentVelocity(1000);
                vel_X.add((double)VelocityTrackerCompat.getXVelocity(vt, pointerId));
                vel_Y.add((double)VelocityTrackerCompat.getYVelocity(vt, pointerId));
                break;
            case MotionEvent.ACTION_UP:
            case MotionEvent.ACTION_CANCEL:
                vt.recycle();
                vt = null;
                break;
        }

        return true;
    }

    public void onPositiveClick (View view){
        String [] to_write = make_csv_able(vel_X, vel_Y, "one");
        File file = new File (getFilesDir(), filename);
        try{
            if(!file.exists()){
               file.createNewFile();
            }
            CSVWriter writer = new CSVWriter(new FileWriter(file, true));
            writer.writeNext(to_write);
            writer.close();
        }catch (Exception e){
            e.printStackTrace();
        }
        TextView tv = (TextView)findViewById(R.id.train_count);
        tv.setText(Integer.toString(++count));
        vel_X = new ArrayList<>();
        vel_Y = new ArrayList<>();
        vt = null;
    }
    public void onNegativeClick (View view){
        String [] to_write = make_csv_able(vel_X, vel_Y, "two");
        File file = new File (getFilesDir(), filename);
        try{
            if(!file.exists()){
                file.createNewFile();
            }
            CSVWriter writer = new CSVWriter(new FileWriter(file, true));
            writer.writeNext(to_write);
            writer.close();
        }catch (Exception e){
            e.printStackTrace();
        }
        TextView tv = (TextView)findViewById(R.id.train_count);
        tv.setText(Integer.toString(++count));
        vel_X = new ArrayList<>();
        vel_Y = new ArrayList<>();
        vt = null;
    }
    public void onFinishedClick (View view){
        String [] to_write = make_csv_able(vel_X, vel_Y, "three");
        File file = new File (getFilesDir(), filename);
        try{
            if(!file.exists()){
                file.createNewFile();
            }
            CSVWriter writer = new CSVWriter(new FileWriter(file, true));
            writer.writeNext(to_write);
            writer.close();
        }catch (Exception e){
            e.printStackTrace();
        }
        TextView tv = (TextView)findViewById(R.id.train_count);
        tv.setText(Integer.toString(++count));
        vel_X = new ArrayList<>();
        vel_Y = new ArrayList<>();
        vt = null;
    }

    private String[] make_csv_able (List<Double> X_vals, List<Double> Y_vals, String category){
        Integer return_size = X_vals.size();
        String [] to_return = new String[return_size + 1];
        to_return[0] = category;

        for (int i = 0; i < return_size; i ++){
            double val = Math.sqrt(Math.pow(X_vals.get(i), 2) + Math.pow(Y_vals.get(i), 2));
            to_return[i+1] = Double.toString(val);
        }

        return to_return;
    }
}
